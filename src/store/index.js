import { store } from "quasar/wrappers";
import { createStore } from "vuex";
import api from "./module/api";
import auth from "./module/auth";
//import common from "./module/common";
import user from "./module/user";
import event from "./module/event";
import combo from "./module/combo";
import home from "./module/home";
import common from "quasar-app-extension-uinws-core/src/store/module/common.js";
import userOrganizer from "./module/userOrganizer";
import bannerCarousel from "./module/bannerCarousel";

// import example from './module-example'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      api,
      auth,
      common,
      home,
      user,
      userOrganizer,
      event,
      combo,
      bannerCarousel,
    },

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING,
  });

  return Store;
});

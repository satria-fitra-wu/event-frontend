import { api } from "boot/axios";
import { Loading, Cookies } from "quasar";
const state = {
};
const mutations = {

};
const actions = {
  async getComboConstantItemListForCombo (
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("/getComboConstantItemListForCombo", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          return response.data.data;
        } else {
          return [];
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });
        return [];
      });
  },
};
const getters = {};
export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};

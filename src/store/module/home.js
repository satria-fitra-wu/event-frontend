import { api } from "boot/axios";
import { Loading, Cookies } from "quasar";
const state = {};

const mutations = {};

const actions = {
  async getFeaturedEvent(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultGetData",
      {
        param: param,
        apiName: "getFeaturedEvent",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async getTopSellingEvent(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultGetDataTable",
      {
        param: param,
        apiName: "getTopSellingEvent",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async getTopEvent(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultGetDataTable",
      {
        param: param,
        apiName: "getTopEvent",
        routePath: "/dashboard/recruitment",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};

import { api } from "boot/axios";
import { Loading } from "quasar";
import { Cookies } from "quasar";
const state = {};
const mutations = {};

const actions = {
  async defaultModifyDataWithMultiFile(
    { commit, dispatch, state, rootGetters, getters },
    { param, apiName, fileArr }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        !Array.isArray(param[key])
          ? formData.append(key, param[key])
          : formData.append(key, JSON.stringify(param[key]));
        //
      }
    }
    fileArr.forEach((val, index, arr) => {
      formData.append("file_upload" + index, val);
    });
    return api
      .post(apiName, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, { root: true });
          return true;
        } else {
          commit("common/showWarning", response.data.data, { root: true });
          return false;
        }
      })
      .catch(function (error) {
        if (error.response != null && error.response.status == 401) {
          commit("auth/setIsLoggedIn", false, { root: true });
          commit("auth/setPermissions", [], { root: true });
          commit("auth/setDisplayName", "", { root: true });
          commit("auth/setRole", "", { root: true });
          Cookies.remove("token", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
          Cookies.remove("role", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        }
        commit("common/showError", error, { root: true });
        return false;
      })
      .finally(function () {});
  },

  async defaultModifyDataWithFile(
    { commit, dispatch, state, rootGetters, getters },
    { param, apiName }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        !Array.isArray(param[key])
          ? formData.append(key, param[key])
          : formData.append(key, JSON.stringify(param[key]));
        //
      }
    }
    return api
      .post(apiName, formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, { root: true });
          return true;
        } else {
          commit("common/showWarning", response.data.data, { root: true });
          return false;
        }
      })
      .catch(function (error) {
        if (error.response != null && error.response.status == 401) {
          commit("auth/setIsLoggedIn", false, { root: true });
          commit("auth/setPermissions", [], { root: true });
          commit("auth/setDisplayName", "", { root: true });
          commit("auth/setRole", "", { root: true });
          Cookies.remove("token", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
          Cookies.remove("role", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        }
        commit("common/showError", error, { root: true });
        return false;
      })
      .finally(function () {});
  },
  async defaultModifyData(
    { commit, dispatch, state, rootGetters, getters },
    { param, apiName }
  ) {
    const formData = new FormData();

    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        !Array.isArray(param[key])
          ? formData.append(key, param[key])
          : formData.append(key, JSON.stringify(param[key]));
        //
      }
    }
    return api
      .post(apiName, formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, { root: true });
          return true;
        } else {
          commit("common/showWarning", response.data.data, { root: true });
          return false;
        }
      })
      .catch(function (error) {
        if (error.response != null && error.response.status == 401) {
          commit("auth/setIsLoggedIn", false, { root: true });
          commit("auth/setPermissions", [], { root: true });
          commit("auth/setDisplayName", "", { root: true });
          commit("auth/setRole", "", { root: true });
          Cookies.remove("token", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
          Cookies.remove("role", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        }
        commit("common/showError", error, { root: true });
        return false;
      })
      .finally(function () {});
  },

  async defaultModifyDataWithReturnResult(
    { commit, dispatch, state, rootGetters, getters },
    { param, apiName }
  ) {
    const formData = new FormData();

    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        !Array.isArray(param[key])
          ? formData.append(key, param[key])
          : formData.append(key, JSON.stringify(param[key]));
        //
      }
    }
    return api
      .post(apiName, formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, { root: true });
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, { root: true });
          return {};
        }
      })
      .catch(function (error) {
        if (error.response != null && error.response.status == 401) {
          commit("auth/setIsLoggedIn", false, { root: true });
          commit("auth/setPermissions", [], { root: true });
          commit("auth/setDisplayName", "", { root: true });
          commit("auth/setRole", "", { root: true });
          Cookies.remove("token", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
          Cookies.remove("role", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        }
        commit("common/showError", error, { root: true });
        return {};
      })
      .finally(function () {});
  },

  async defaultModifyDataWithRoute(
    { commit, dispatch, state, rootGetters, getters },
    { param, apiName, routePath }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        !Array.isArray(param[key])
          ? formData.append(key, param[key])
          : formData.append(key, JSON.stringify(param[key]));
        //
      }
    }
    return api
      .post(apiName, formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, { root: true });
          router.replace(routePath);
          return true;
        } else {
          commit("common/showWarning", response.data.data, { root: true });
          return false;
        }
      })
      .catch(function (error) {
        if (error.response != null && error.response.status == 401) {
          commit("auth/setIsLoggedIn", false, { root: true });
          commit("auth/setPermissions", [], { root: true });
          commit("auth/setDisplayName", "", { root: true });
          commit("auth/setRole", "", { root: true });
          Cookies.remove("token", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
          Cookies.remove("role", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        }
        commit("common/showError", error, { root: true });
        return false;
      })
      .finally(function () {});
  },

  async defaultGetDataTable(
    { commit, dispatch, state, rootGetters, getters },
    { param, apiName }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
      }
    }
    return api
      .post(apiName, param, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          return response.data.data;
        } else {
          return [];
        }
      })
      .catch(function (error) {
        if (error.response != null && error.response.status == 401) {
          commit("auth/setIsLoggedIn", false, { root: true });
          commit("auth/setPermissions", [], { root: true });
          commit("auth/setDisplayName", "", { root: true });
          commit("auth/setRole", "", { root: true });
          Cookies.remove("token", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
          Cookies.remove("role", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        }
        commit("common/showError", error, { root: true });
        return [];
      });
  },

  async defaultCountGetDataTable(
    { commit, dispatch, state, rootGetters, getters },
    { param, apiName }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
      }
    }
    return api
      .post(apiName, formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          return response.data.data;
        } else {
          return 0;
        }
      })
      .catch(function (error) {
        if (error.response != null && error.response.status == 401) {
          commit("auth/setIsLoggedIn", false, { root: true });
          commit("auth/setPermissions", [], { root: true });
          commit("auth/setDisplayName", "", { root: true });
          commit("auth/setRole", "", { root: true });
          Cookies.remove("token", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
          Cookies.remove("role", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        }
        commit("common/showError", error, { root: true });
        return 0;
      });
  },

  async defaultGetData(
    { commit, dispatch, state, rootGetters, getters },
    { param, apiName }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
      }
    }
    return api
      .post(apiName, param, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, { root: true });
          return {};
        }
      })
      .catch(function (error) {
        if (error.response != null && error.response.status == 401) {
          commit("auth/setIsLoggedIn", false, { root: true });
          commit("auth/setPermissions", [], { root: true });
          commit("auth/setDisplayName", "", { root: true });
          commit("auth/setRole", "", { root: true });
          Cookies.remove("token", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
          Cookies.remove("role", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        }
        commit("common/showError", error, { root: true });
        return {};
      });
  },

  async defaultDownloadData(
    { commit, dispatch, state, rootGetters, getters },
    { param, apiName }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
      }
    }
    return api
      .get(apiName, {
        params: param,
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
        responseType: "blob",
        //responseType: "arraybuffer",
      })
      .then(async function (response) {
        let responsseJson = {};
        if (response.data.type == "application/json") {
          responsseJson = JSON.parse(await response.data.text());
          if (!responsseJson.success) {
            commit("common/showWarning", responsseJson.data, { root: true });
            return {};
          }
        }
        const url = window.URL.createObjectURL(new Blob([response.data]), {
          type: response.headers["content-type"],
        });
        const link = document.createElement("a");
        link.href = url;
        //link.target = "_blank";
        link.setAttribute(
          "download",
          response.headers["content-disposition"].split("filename=")[1]
        );
        document.body.appendChild(link);
        link.click();
        window.URL.revokeObjectURL(url);
        //FileSaver.saveAs(new Blob([response.data]));
      })
      .catch(function (error) {
        if (error.response != null && error.response.status == 401) {
          commit("auth/setIsLoggedIn", false, { root: true });
          commit("auth/setPermissions", [], { root: true });
          commit("auth/setDisplayName", "", { root: true });
          commit("auth/setRole", "", { root: true });
          Cookies.remove("token", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
          Cookies.remove("role", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        }
        commit("common/showError", error, { root: true });
        return {};
      });
  },
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};

import { api } from "boot/axios";
import { Loading, Cookies } from "quasar";
const state = {
  username: "",
  profile: {},
  permissions: [],
};

const mutations = {
  setUsername(state, value) {
    state.username = value;
  },
  setProfile(state, value) {
    state.profile = value;
  },
  setpermissions(state, value) {
    state.permissions = value;
  },
};

const actions = {
  async addEvent(
    { commit, dispatch, state, rootGetters, getters },
    { event, dateList, speakerList, ticketList, locationList }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in event) {
      if (event.hasOwnProperty(key)) {
        formData.append(key, event[key]);
        //
      }
    }
    speakerList.forEach((element, index) => {
      if (element.speaker_photo != null) {
        formData.append("speakerPhoto" + index, element.speaker_photo);
        element.flg_speaker_photo = "1";
      } else {
        element.flg_speaker_photo = "0";
      }
    });
    formData.append("dateList", JSON.stringify(dateList));
    formData.append("speakerList", JSON.stringify(speakerList));
    formData.append("ticketList", JSON.stringify(ticketList));
    formData.append("locationList", JSON.stringify(locationList));
    return api
      .post("addEvent", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          router.push({ path: "/admin/event" });
          return true;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },
  async editEvent(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("editEvent", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          router.push({ path: "/admin/event" });
          return true;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async editEventActiveStatus(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyData",
      {
        param: param,
        apiName: "editEventActiveStatus",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
  async addEventDate(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyData",
      {
        param: param,
        apiName: "addEventDate",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
  async addSpeaker(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("addSpeaker", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async editSpeaker(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("editSpeaker", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async addTicket(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("addTicket", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async editTicket(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("editTicket", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },
  async addLocation(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("addLocation", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async editLocation(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("editLocation", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async addSession(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        if (key == "ticket_list") {
          formData.append("ticket_list[]", param[key]);
        } else {
          formData.append(key, param[key]);
        }
        //
      }
    }
    return api
      .post("addSession", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async editSession(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        if (key == "ticket_list") {
          formData.append("ticket_list[]", param[key]);
        } else {
          formData.append(key, param[key]);
        }
        //
      }
    }
    return api
      .post("editSession", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async findEventSpeakerById(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("findEventSpeakerById", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return {};
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return {};
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async findEventTicketById(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("findEventTicketById", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return {};
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return {};
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async findEventLocationById(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("findEventLocationById", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return {};
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return {};
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async findEventSessionById(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("findEventSessionById", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          return response.data.data;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return {};
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return {};
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async uploadPayment(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key]);
        //
      }
    }
    return api
      .post("/uploadPayment", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return true;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });
        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async orderTicket(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
        //
      }
    }
    return api
      .post("/orderTicket", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return true;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },

  async verifyPayment(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    Loading.show();
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
        //
      }
    }
    return api
      .post("/verifyPayment", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then(function (response) {
        if (response.data.success) {
          commit("common/showSuccess", response.data.data.msg, {
            root: true,
          });
          return true;
        } else {
          commit("common/showWarning", response.data.data, {
            root: true,
          });
          return false;
        }
      })
      .catch(function (error) {
        commit("common/showError", error, { root: true });

        return false;
      })
      .finally(function () {
        Loading.hide();
      });
  },
  async getEventByAdvance(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
        //
      }
    }
    return api
      .post("getEventByAdvance", formData)
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return [];
        }
      })
      .catch(function (error) {
        return [];
      })
      .finally(function () {});
  },

  async countGetEventByAdvance(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
      }
    }
    return api
      .post("countGetEventByAdvance", formData)
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return 0;
        }
      })
      .catch(function (error) {
        return 0;
      })
      .finally(function () {});
  },

  async findEventById(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultGetData",
      {
        param: param,
        apiName: "findEventById",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async getDetailEvent(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
        //
      }
    }
    return api
      .post("/getDetailEvent", formData)
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return [];
        }
      })
      .catch(function (error) {
        return [];
      })
      .finally(function () {});
  },

  async getTicketListForCombo(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
        //
      }
    }
    return api
      .post("/getTicketListForCombo", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return [];
        }
      })
      .catch(function (error) {
        return [];
      })
      .finally(function () {});
  },

  async getTicketListForBuy(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
        //
      }
    }
    return api
      .post("/getTicketListForBuy", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return [];
        }
      })
      .catch(function (error) {
        return [];
      })
      .finally(function () {});
  },
  async getMyTicket(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
        //
      }
    }
    return api
      .post("/getMyTicket", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return [];
        }
      })
      .catch(function (error) {
        return [];
      })
      .finally(function () {});
  },

  async countGetMyTicket(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
      }
    }
    return api
      .post("/countGetMyTicket", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return 0;
        }
      })
      .catch(function (error) {
        return 0;
      })
      .finally(function () {});
  },

  async getMyEvent(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
        //
      }
    }
    return api
      .post("/getMyEvent", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return [];
        }
      })
      .catch(function (error) {
        return [];
      })
      .finally(function () {});
  },

  async countGetMyEvent(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
      }
    }
    return api
      .post("/countGetMyEvent", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return 0;
        }
      })
      .catch(function (error) {
        return 0;
      })
      .finally(function () {});
  },

  async getTicketSales(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
        //
      }
    }
    return api
      .post("/getTicketSales", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return [];
        }
      })
      .catch(function (error) {
        return [];
      })
      .finally(function () {});
  },

  async countGetTicketSales(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const router = this.$router;
    const formData = new FormData();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {
        formData.append(key, param[key] == null ? "" : param[key]);
      }
    }
    return api
      .post("/countGetTicketSales", formData, {
        headers: {
          Authorization: Cookies.has("token")
            ? "Bearer " + Cookies.get("token")
            : "",
        },
      })
      .then((response) => {
        if (response.data.success) {
          return response.data.data;
        } else {
          return 0;
        }
      })
      .catch(function (error) {
        return 0;
      })
      .finally(function () {});
  },

  async countGetEventByFilterList(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    //Loading.show();
    const result = await dispatch(
      "api/defaultCountGetDataTable",
      {
        param: param,
        apiName: "countGetEventByFilterList",
      },
      { root: true }
    );
    //Loading.hide();
    return result;
  },

  async getEventByFilterList(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    //Loading.show();
    const result = await dispatch(
      "api/defaultGetDataTable",
      {
        param: param,
        apiName: "getEventByFilterList",
      },
      { root: true }
    );
    //Loading.hide();
    return result;
  },

  async findEventDateById(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultGetData",
      {
        param: param,
        apiName: "findEventDateById",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async getEventLocationListByEventTicketOrderId(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const result = await dispatch(
      "api/defaultGetDataTable",
      {
        param: param,
        apiName: "getEventLocationListByEventTicketOrderId",
      },
      { root: true }
    );
    return result;
  },

  async editEventDate(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyDataWithReturnResult",
      {
        param: param,
        apiName: "editEventDate",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
  async deleteEventDate(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyDataWithReturnResult",
      {
        param: param,
        apiName: "deleteEventDate",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
  async deleteEventSpeaker(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyDataWithReturnResult",
      {
        param: param,
        apiName: "deleteEventSpeaker",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
  async deleteEventTicket(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyDataWithReturnResult",
      {
        param: param,
        apiName: "deleteEventTicket",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
  async deleteEventLocation(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyDataWithReturnResult",
      {
        param: param,
        apiName: "deleteEventLocation",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async uploadTemplateCertificate(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyDataWithFile",
      {
        param: param,
        apiName: "uploadTemplateCertificate",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
  async addCertificateParameter(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyData",
      {
        param: param,
        apiName: "addCertificateParameter",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async editCertificateParameter(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyData",
      {
        param: param,
        apiName: "editCertificateParameter",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async deleteCertificateParameter(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyDataWithFile",
      {
        param: param,
        apiName: "deleteCertificateParameter",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async findEventCertificateParameterById(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultGetData",
      {
        param: param,
        apiName: "findEventCertificateParameterById",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
  async downloadCertificate(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultDownloadData",
      {
        param: param,
        apiName: "downloadCertificate",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async downloadCertificateForUser(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultDownloadData",
      {
        param: param,
        apiName: "downloadCertificateForUser",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
};

const getters = {
  token(state) {
    return state.token;
  },

  username(state) {
    return state.username;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};

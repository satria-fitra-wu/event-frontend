import { api } from "boot/axios";
import { Loading, Cookies } from "quasar";
const state = {
  username: "",
  profile: {},
  permissions: [],
};

const mutations = {
  setUsername(state, value) {
    state.username = value;
  },
  setProfile(state, value) {
    state.profile = value;
  },
  setpermissions(state, value) {
    state.permissions = value;
  },
};

const actions = {
  async register({ commit, dispatch, state, rootGetters, getters }, { param }) {
    const router = this.$router;
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyData",
      {
        param: param,
        apiName: "register",
      },
      { root: true }
    );
    Loading.hide();
    if (result) {
      router.push({ path: "/login" });
    }
    return result;

    // return api
    //   .post(rootGetters["api/registerAPI"], register)
    //   .then(function (response) {
    //     console.log("response :", response);
    //     if (response.data.success) {
    //       commit("common/showSuccess", response.data.data.msg, { root: true });
    //       router.push({ path: "/login" });
    //       return true;
    //     } else {
    //       commit("common/showWarning", response.data.data, { root: true });
    //       return false;
    //     }
    //   })
    //   .catch(function (error) {
    //     console.log(error);
    //     commit("common/showError", error, { root: true });

    //     return false;
    //   })
    //   .finally(function () {
    //     Loading.hide();
    //   });
  },
  async sendVerificationEmail(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyData",
      {
        param: param,
        apiName: "sendVerificationEmail",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
};
const getters = {
  token(state) {
    return state.token;
  },

  username(state) {
    return state.username;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};

import { api } from "boot/axios";
import { Loading, Cookies } from "quasar";
const state = {};

const mutations = {};

const actions = {
  async addBannerCarousel(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyDataWithFile",
      {
        param: param,
        apiName: "addBannerCarousel",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
  async editBannerCarousel(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyDataWithFile",
      {
        param: param,
        apiName: "editBannerCarousel",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async deleteBannerCarousel(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyData",
      {
        param: param,
        apiName: "deleteBannerCarousel",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async getBannerCarouselList(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const result = await dispatch(
      "api/defaultGetDataTable",
      {
        param: param,
        apiName: "getBannerCarouselList",
      },
      { root: true }
    );
    return result;
  },

  async countGetBannerCarouselList(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    const result = await dispatch(
      "api/defaultCountGetDataTable",
      {
        param: param,
        apiName: "countGetBannerCarouselList",
      },
      { root: true }
    );
    return result;
  },

  async findBannerCarouselById(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultGetData",
      {
        param: param,
        apiName: "findBannerCarouselById",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};

import { api } from "boot/axios";
import { Loading, Cookies } from "quasar";
const state = {};

const mutations = {};

const actions = {
  async addUserOrganizer(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyData",
      {
        param: param,
        apiName: "addUserOrganizer",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
  async getProfile(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultGetData",
      {
        param: param,
        apiName: "getProfile",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },

  async editProfile(
    { commit, dispatch, state, rootGetters, getters },
    { param }
  ) {
    Loading.show();
    const result = await dispatch(
      "api/defaultModifyData",
      {
        param: param,
        apiName: "editProfile",
      },
      { root: true }
    );
    Loading.hide();
    return result;
  },
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};

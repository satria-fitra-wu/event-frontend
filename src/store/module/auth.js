import { Loading, LocalStorage } from "quasar";
import { Cookies } from "quasar";
import { api } from "boot/axios";
const state = {
  isLoggedIn: false,
  displayName: "",
  role: "",
  lang: "en",
  verified: false,
  redirectToEmailNotVerified: false,
};

const mutations = {
  setPermissions(state, value) {
    LocalStorage.set("permissions", value);
  },
  setIsLoggedIn(state, value) {
    state.isLoggedIn = value;
  },
  setDisplayName(state, value) {
    state.displayName = value;
  },
  setRole(state, value) {
    state.role = value;
  },
  setLang(state, value) {
    state.lang = value;
  },
  setVerified(state, value) {
    state.verified = value;
  },

  setRedirectToEmailNotVerified(state, value) {
    state.redirectToEmailNotVerified = value;
  },
};

const actions = {
  async login({ commit, dispatch, state, rootGetters, getters }, { param }) {
    const router = this.$router;
    try {
      Loading.show();
      return api
        .post("login", param)
        .then(function (response) {
          if (response.data.success) {
            commit("common/showSuccess", response.data.data.msg, {
              root: true,
            });

            commit("setIsLoggedIn", true);
            commit("setPermissions", response.data.data.permissions);
            // LocalStorage.set("permissions", response.data.data.permissions);
            // console.log(
            //   "localstorage login :",
            //   LocalStorage.getItem("permissions")
            // );
            commit("setDisplayName", response.data.data.display_name);
            commit("setRole", response.data.data.role);
            true;
            Cookies.set("role", response.data.data.role, {
              expires: "24h",
              sameSite: "Strict",
              secure: true,
              path: "/",
            });
            Cookies.set("token", response.data.data.token, {
              expires: "24h",
              sameSite: "Strict",
              secure: true,
              path: "/",
            });

            if (response.data.data.verified) {
              router.replace({ path: "/" });
            } else {
              commit("setRedirectToEmailNotVerified", true);
              router.replace({ path: "/emailNotVerified/" });
            }
          } else {
            commit("common/showWarning", response.data.data, {
              root: true,
            });
          }
        })
        .catch(function (error) {
          commit("common/showError", error, { root: true });
        })
        .finally(function () {
          Loading.hide();
        });
    } catch (error) {}
  },
  async logout(
    { commit, dispatch, state, rootGetters, getters },
    { username, password }
  ) {
    const router = this.$router;
    try {
      Loading.show();
      return api
        .post(
          "logout",
          {},
          {
            headers: {
              Authorization: Cookies.has("token")
                ? "Bearer " + Cookies.get("token")
                : "",
            },
          }
        )
        .then(function (response) {
          if (response.data.success) {
            commit("common/showSuccess", response.data.data.msg, {
              root: true,
            });
            commit("setIsLoggedIn", false);
            commit("setPermissions", []);
            commit("setDisplayName", "");
            commit("setRole", "");

            Cookies.remove("token", {
              sameSite: "Strict",
              secure: true,
              path: "/",
            });
            Cookies.remove("role", {
              sameSite: "Strict",
              secure: true,
              path: "/",
            });
            router.replace({ path: "/" });
          } else {
            commit("common/showWarning", response.data.data, {
              root: true,
            });
          }
        })
        .catch(function (error) {
          commit("common/showError", error, { root: true });
        })
        .finally(function () {
          Loading.hide();
        });
    } catch (error) {}
  },

  async isTokenValid(
    { commit, dispatch, state, rootGetters, getters },
    { token }
  ) {
    const router = this.$router;
    Loading.show();
    return api
      .post(
        "isTokenValid",
        {},
        {
          headers: {
            Authorization: Cookies.has("token")
              ? "Bearer " + Cookies.get("token")
              : "",
          },
        }
      )
      .then(function (response) {
        if (response.data.success) {
          //console.log("masuk set true", response);
          commit("setIsLoggedIn", true);
          commit("setPermissions", response.data.data.permissions);
          commit("setDisplayName", response.data.data.display_name);
          commit("setRole", response.data.data.role);
          commit("setVerified", response.data.data.verified);
          Cookies.set("role", response.data.data.role, {
            expires: "24h",
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        } else {
          if (response.data.errorKey == 900) {
            commit("setRedirectToEmailNotVerified", true);
          } else {
            commit("setIsLoggedIn", false);
            commit("setPermissions", []);
            commit("setDisplayName", "");
            commit("setRole", "");
            Cookies.remove("token", {
              sameSite: "Strict",
              secure: true,
              path: "/",
            });
            Cookies.remove("role", {
              sameSite: "Strict",
              secure: true,
              path: "/",
            });
          }
        }
      })
      .catch(function (error) {
        if (error.response.data.errorKey == 900) {
          commit("setRedirectToEmailNotVerified", true);
        } else {
          commit("setIsLoggedIn", false);
          commit("setPermissions", []);
          commit("setDisplayName", "");
          commit("setRole", "");
          Cookies.remove("token", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
          Cookies.remove("role", {
            sameSite: "Strict",
            secure: true,
            path: "/",
          });
        }
      })
      .finally(function () {
        Loading.hide();
      });
  },
};
const getters = {
  token(state) {
    return state.token;
  },
  displayName(state) {
    return state.displayName;
  },

  role(state) {
    return state.role;
  },
  isLoggedIn(state) {
    return state.isLoggedIn;
  },
  lang(state) {
    return state.lang;
  },
  verified(state) {
    return state.verified;
  },
  redirectToEmailNotVerified(state) {
    return state.redirectToEmailNotVerified;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};

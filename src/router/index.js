import { route } from "quasar/wrappers";
import {
  createRouter,
  createMemoryHistory,
  createWebHistory,
  createWebHashHistory,
} from "vue-router";
import routes from "./routes";
import { Cookies } from "quasar";
import isPermit from "quasar-app-extension-uinws-core/src/helper/permission-helper.js";

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default route(async function ({ store, ssrContext }) {
  const createHistory = process.env.SERVER
    ? createMemoryHistory
    : process.env.VUE_ROUTER_MODE === "history"
    ? createWebHistory
    : createWebHashHistory;

  const Router = createRouter({
    scrollBehavior: () => ({ left: 0, top: 0 }),
    routes,

    // Leave this as is and make changes in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    history: createHistory(
      process.env.MODE === "ssr" ? void 0 : process.env.VUE_ROUTER_BASE
    ),
  });

  if (
    !store.getters["auth/isLoggedIn"] &&
    Cookies.get("token") != null &&
    Cookies.get("token") != ""
  ) {
    await store.dispatch("auth/isTokenValid", { token: Cookies.get("token") });
  }
  //const store = useStore();
  Router.beforeEach((to, from, next) => {
    var redirectToLogin = false;
    var redirect = false;
    var redirectToEmailNotVerified =
      store.getters["auth/redirectToEmailNotVerified"];
    if (Array.isArray(to.meta.permissions) && !isPermit(to.meta.permissions)) {
      redirect = true;
    }
    if (redirect) {
      next({ path: "/notAuthorized" });
    } else {
      to.matched.some((record) => {
        if (record.meta.requiresAuth) {
          let isLoggedIn = store.getters["auth/isLoggedIn"];
          if (!isLoggedIn) {
            redirectToLogin = true;
          }
        }
      });
      if (redirectToEmailNotVerified) {
        store.commit("auth/setRedirectToEmailNotVerified", false);
        next({ path: "/emailNotVerified" });
      } else {
        if (redirectToLogin) {
          next({ path: "/login" });
        } else {
          next();
        }
      }
    }
  });

  return Router;
});

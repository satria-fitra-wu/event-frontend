const routes = [
  {
    path: "/admin",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      {
        path: "/admin/search",
        component: () => import("pages/Index.vue"),
      },
      {
        path: "/bannerCarousel",
        component: () => import("pages/bannerCarousel/BannerCarousel.vue"),
      },
      {
        path: "/admin/event",
        component: () => import("src/pages/event/Event.vue"),
        meta: { requiresAuth: true },
      },
      {
        path: "/admin/event/addEvent",
        component: () => import("src/pages/event/AddEvent.vue"),
        meta: { requiresAuth: true },
      },
      {
        path: "/admin/event/editEvent/:eventId",
        component: () => import("src/pages/event/EditEvent.vue"),
        meta: { requiresAuth: true },
      },
      {
        path: "/admin/event/ticketSales/:eventCode",
        component: () => import("src/pages/event/TicketSales.vue"),
        meta: { requiresAuth: true },
      },

      {
        path: "/myTicket",
        meta: { requiresAuth: true },
        component: () => import("pages/MyTicket.vue"),
      },
      { path: "/register", component: () => import("pages/Register.vue") },
    ],
  },
  { path: "/login", component: () => import("pages/Login.vue") },
  {
    path: "/emailNotVerified",
    component: () => import("pages/EmailNotVerified.vue"),
  },
  {
    path: "/failActivateEmail/:param",
    component: () => import("pages/FailActivateEmail.vue"),
  },
  {
    path: "/successActivateEmail",
    component: () => import("pages/SuccessActivateEmail.vue"),
  },
  {
    path: "/event/:eventCode",
    component: () => import("layouts/DetailEventLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("pages/DetailEvent.vue"),
      },
    ],
  },
  {
    path: "/",
    component: () => import("layouts/HomeLayout.vue"),
    children: [
      {
        path: "",
        component: () => import("src/pages/Home.vue"),
      },
      {
        path: "/search",
        component: () => import("pages/Search.vue"),
      },
      {
        path: "/eventByOrganizer/:organizerName",
        component: () => import("pages/SearchByOrganizer.vue"),
      },
      {
        path: "/about",
        component: () => import("pages/About.vue"),
      },
      {
        path: "/terms",
        component: () => import("pages/Terms.vue"),
      },
      {
        path: "/contact",
        component: () => import("pages/Contact.vue"),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "/:catchAll(.*)*",
    component: () => import("pages/Error404.vue"),
  },
];

export default routes;
